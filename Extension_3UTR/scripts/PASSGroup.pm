#!/usr/bin/perl

# PASSGroup
#
# meta data class to wrap info
# for PolyA supported site description
#
# required by PASSCluster.
#
# Version 1.0
# Date Nov 2016
# Georgi Tushev
# Scientific Computing Facility
# Max-Planck Institute for Brain Research
# send bug reports to sciclist@brain.mpg.de
#

package PASSGroup;
require PASSRecord;

sub new
{
    my $class = shift;
    
    my $self = {};
    bless($self, $class);
    
    $self->{chrom} = "";
    $self->{start} = -1;
    $self->{end} = -1;
    $self->{id} = 0;
    $self->{span} = 0;
    $self->{strand} = "";
    $self->{masked} = 0;
    
    $self->{reads}{count} = 0;
    $self->{reads}{sum} = 0;
    $self->{reads}{max} = 0;
    $self->{reads}{best} = -1;
    
    $self->{tails}{count} = 0;
    $self->{tails}{sum} = 0;
    $self->{tails}{max} = 0;
    $self->{tails}{best} = -1;
    
    return $self;
}


sub create
{
    my $self = shift;
    my $ref = shift;
    my $id = shift;
    
    $self->{chrom} = $ref->{chrom};
    $self->{start} = $ref->{base} - 1;
    $self->{end} = $ref->{base};
    $self->{id} = $id;
    $self->{span} = 1;
    $self->{strand} = $ref->{strand};
    $self->{masked} = $ref->{poly};
    
    $self->{reads}{count} = 1;
    $self->{reads}{sum} = $ref->{reads};
    $self->{reads}{max} = $ref->{reads};
    $self->{reads}{best} = $ref->{base};
    
    if ($ref->ispass() == 1)
    {
        $self->{tails}{count} = 1;
        $self->{tails}{sum} = $ref->{tails};
        $self->{tails}{max} = $ref->{tails};
        $self->{tails}{best} = $ref->{base};
    }
    else
    {
        $self->{tails}{count} = 0;
        $self->{tails}{sum} = 0;
        $self->{tails}{max} = 0;
        $self->{tails}{best} = -1;
    }
    
    return $self;
}

sub extend
{
    my $self = shift;
    my $ref = shift;
    
    $self->{end} = $ref->{base};
    $self->{span} = $self->{end} - $self->{start};
    $self->{masked} += $ref->{poly};
    
    $self->{reads}{count}++;
    $self->{reads}{sum} += $ref->{reads};
    
    # check is best
    my $reads_isbest = ($self->{strand} eq "+") ? ($self->{reads}{max} <= $ref->{reads}) : ($self->{reads}{max} < $ref->{reads});
    if ($reads_isbest)
    {
        $self->{reads}{best} = $ref->{base};
        $self->{reads}{max} = $ref->{reads};
    }
    
    # update tails
    if ($ref->ispass() == 1)
    {
        $self->{tails}{count}++;
        $self->{tails}{sum} += $ref->{tails};
        
        my $tails_isbest = ($self->{strand} eq "+") ? ($self->{tails}{max} <= $ref->{tails}) : ($self->{tails}{max} < $ref->{tails});
        if ($tails_isbest)
        {
            $self->{tails}{best} = $ref->{base};
            $self->{tails}{max} = $ref->{tails};
        }
        
    }
    
    return $self;
}

sub hastail
{
    my $self = shift;
    
    my $hastail = ($self->{tails}{count} > 0) ? 1 : 0;
    
    return $hastail;
}


sub report
{
    my $self = shift;
    
    return if ($self->{start} == -1);
    
    my $name = ($self->{strand} eq "+") ? sprintf("fwdgroup%08d", $self->{id}) : sprintf("revgroup%08d", $self->{id});
    
    print $self->{chrom},"\t",$self->{start},"\t",$self->{end},"\t",$name,"\t",$self->{span},"\t",$self->{strand},"\t",$self->{masked},"\t";
    print $self->{reads}{count},"\t",$self->{reads}{sum},"\t",$self->{reads}{max},"\t",$self->{reads}{best},"\t";
    print $self->{tails}{count},"\t",$self->{tails}{sum},"\t",$self->{tails}{max},"\t",$self->{tails}{best},"\n";
}



1; #return true
