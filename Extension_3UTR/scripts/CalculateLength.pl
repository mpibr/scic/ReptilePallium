#!/usr/bin/perl

use warnings;
use strict;
use IO::Zlib;

sub ParseAnnotation($$);
sub ParsePass($$);

MAIN:
{
    my $file_annotation = shift;
    my $file_pass = shift;
    my %data = ();
    
    ParseAnnotation($file_annotation, \%data);
    ParsePass($file_pass, \%data);
    
    exit 0;
}

sub ParseAnnotation($$)
{
    my $file_annotation = $_[0];
    my $data_ref = $_[1];
    
    my $fh = IO::Zlib->new($file_annotation, "rb");
    if (defined($fh))
    {
        while(<$fh>)
        {
            chomp($_);
            
            my @bed = split("\t", $_, 12);
            my @name = split(";", $bed[3], 2);
            my $key = $bed[0] . ";" . $name[1] . ";" . $bed[5];
            
            my $tpend = ($bed[5] eq "+") ? $bed[2] : $bed[1];
            push(@{$data_ref->{$key}}, $tpend);
            
        }
        $fh->close;
    }
    
}

sub ParsePass($$)
{
    my $file_pass = $_[0];
    my $data_ref = $_[1];
    my $line = 0;
    
    my $fh = IO::Zlib->new($file_pass, "rb");
    if (defined($fh))
    {
        while(<$fh>)
        {
            chomp($_);
            
            my @pass = split("\t", $_, 20);
            next if($pass[-1] == 0);
            next if($pass[16] =! m/3pUTR/g);
            
            ### calculate distance
            my $pkey = $pass[0] . ";" . $pass[15] . ";" . $pass[5];
            
            next if(!exists($data_ref->{$pkey}));
            
            my @dlng = @{$data_ref->{$pkey}};
            my @dspan = ();
            my @rspan = ();
            foreach my $tpend (@dlng)
            {
                push(@rspan, abs($tpend - $pass[14]));
                push(@dspan, $pass[14] - $tpend);
            }
            
            
            my @sorted_indexes = sort {$rspan[$a] <=> $rspan[$b]} 0..$#rspan;
            @dspan = @dspan[@sorted_indexes];
            
            
            
            
            print $pkey,"\t", $dspan[0],"\n" if(abs($dspan[0]) <= 10000);
            
            
            
            
        }
        $fh->close;
    }
    print $line,"\n";
    
}

