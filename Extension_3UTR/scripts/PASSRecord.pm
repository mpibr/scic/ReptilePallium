#!/usr/bin/perl

# PASSRecord
#
# meta data class to wrap info
# for PolyA supported site record
#
# required by PASSCluster.
#
# Version 1.0
# Date Nov 2016
# Georgi Tushev
# Scientific Computing Facility
# Max-Planck Institute for Brain Research
# send bug reports to sciclist@brain.mpg.de
#

package PASSRecord;

sub new
{
    my $class = shift;
    
    my $self = {};
    
    bless($self, $class);
    
    $self->{chrom} = "";
    $self->{strand} = "";
    $self->{base} = -1;
    $self->{reads} = -1;
    $self->{tails} = -1;
    $self->{poly} = -1;
    
    return $self;
}

sub parse
{
    my $self = shift;
    my $line = shift;
    my ($chrom, $base, $strand, $reads, $tails, $poly) = split("\t", $line, 6);
    $self->{chrom} = $chrom;
    $self->{strand} = $strand;
    $self->{base} = $base;
    $self->{reads} = $reads;
    $self->{tails} = $tails;
    $self->{poly} = $poly;
    
    return $self;
}

sub update
{
    my $self = shift;
    my $qry = shift;
    
    $self->{chrom} = $qry->{chrom};
    $self->{strand} = $qry->{strand};
    $self->{base} = $qry->{base};
    $self->{reads} = $qry->{reads};
    $self->{tails} = $qry->{tails};
    $self->{poly} = $qry->{poly};

    return $self;
}

sub isreverse
{
    my $self = shift;
    
    my $isreverse = ($self->{strand} eq "+") ? 1 : 0;
    
    return $isreverse;
}

sub ispass
{
    my $self = shift;
    
    my $ispass = 1;
    
    # condition tail containing read
    $ispass = 0 if($self->{tails} == 0);
    
    # condition poly mask region
    $ispass = 0 if($self->{poly} == 1);
    
    return $ispass;
}

sub isoverlap
{
    my $self = shift;
    my $prev = shift;
    my $window = shift;
    
    my $isoverlap = 1;
    
    # check strand
    $isoverlap = 0 if($self->{strand} ne $prev->{strand});
    
    # check chromosome
    $isoverlap = 0 if($self->{chrom} ne $prev->{chrom});
    
    # check base offset
    $isoverlap = 0 if(abs($self->{base} - $prev->{base}) > $window);
    
    return $isoverlap;
}

sub report
{
    my $self = shift;
    print $self->{chrom},"\t",$self->{base},"\t",$self->{strand},"\t",$self->{reads},"\t",$self->{tails},"\t",$self->{poly},"\n";
}

1; # return true