#!/usr/bin/perl

use warnings;
use strict;
use IO::Zlib;

sub ParsePass($);

MAIN:
{
    my $file_pass = shift;

    ParsePass($file_pass);
    
    exit 0;
}


sub ParsePass($)
{
    my $file_pass = $_[0];
    my $data_ref = $_[1];
    
    my $fh = IO::Zlib->new($file_pass, "rb");
    if (defined($fh))
    {
        while(<$fh>)
        {
            chomp($_);
            
            my @pass = split("\t", $_);
            
            
            if (($pass[19]==1) && (index($pass[16],"_extended") > -1))
            {
                my $name = $pass[3] . ";" . $pass[15] . ";" . $pass[16];
                my $chromStart = $pass[17];
                my $chromEnd = $pass[14];
                
                if ($pass[14] < $pass[17])
                {
                    $chromStart = $pass[14];
                    $chromEnd = $pass[17];
                }
                
                print $pass[0],"\t",$chromStart,"\t",$chromEnd,"\t",$name,"\t",($chromEnd - $chromStart),"\t",$pass[5],"\n";
                
            }
            
        }
        $fh->close;
    }
    
    
}

