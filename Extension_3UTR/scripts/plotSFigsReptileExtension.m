% plotSFigsReptileExtension
clc
clear variables
close all


%% read 3'UTR_extended length
fr = fopen('passLength_Turtle_05Oct2017.txt','r');
txt = textscan(fr, '%*s %n', 'delimiter', '\t');
fclose(fr);
turtleLength = txt{1};

fr = fopen('passLength_Lizard_05Oct2017.txt','r');
txt = textscan(fr, '%*s %n', 'delimiter', '\t');
fclose(fr);
lizardLength = txt{1};


%% calculate ECDF
[turtleECDF,turtleX] = ecdf(turtleLength);
[lizardECDF,lizardX] = ecdf(lizardLength);



xuq = 68;
yuq = mean([turtleECDF(find(turtleX>xuq,1));...
            lizardECDF(find(lizardX>xuq,1))]);
%xuq = prctile([turtleLength;lizardLength], 75);



figure('color','w');
hold on;
plot([-10000,xuq],[yuq,yuq],'-.','color',[.65,.65,.65]);
plot([xuq,xuq],[0,yuq],'-.','color',[.65,.65,.65]);
h(1) = plot(turtleX, turtleECDF,'k','LineWidth', 1.2);
h(2) = plot(lizardX, lizardECDF,'color',[.65,.65,.65],'LineWidth', 1.2);
hold off;
text(-9500,yuq-0.025,'annotated 3''UTRs',...
    'fontsize',10,'horizontalalignment','left');
text(-9500,yuq+0.025,'extended 3''UTRs',...
    'fontsize',10,'horizontalalignment','left');
text(xuq+100,0.1,sprintf('read length\n68 nts'),...
    'fontsize',10,'horizontalalignment','left',...
    'verticalalignment','top');
hl = legend(h,'turtle','lizard');
set(hl,'edgecolor','w','location','southeast');

set(gca,'XTickLabel',-10:2:10);
xlabel('relative distance from annotated 3''End [kB]');
ylabel('relative fraction');
print(gcf,'-dpng','-r300','figureX_relativeExtension.png');
print(gcf,'-dsvg','-r300','figureX_relativeExtension.svg');


