% plotDropSeq_Features
clc
clear variables
close all

fileFeatures = '/Volumes/Data/External/Bioinformatics/Projects/DropSeq/processed/tableFeatures_06Oct2017.txt';
fr = fopen(fileFeatures, 'r');
hdr = fgetl(fr);
hdr = regexp(hdr,'\t','split');
fmt = repmat({'%n'},length(hdr),1);
fmt(1:2) = {'%s'};
fmt = sprintf('%s ',fmt{:});
fmt(end) = [];
txt = textscan(fr, fmt, 'delimiter', '\t');
fclose(fr);
species = txt{1};
samples = txt{2};
counts = [txt{3:end}];

%% Reads from good cells associated with features
ftr = bsxfun(@rdivide, 100.*counts(:,4:end),counts(:,3));
lbl = hdr(6:end);
lbl([2,4]) = [];
lbl = regexprep(lbl,'\_','-');
ftr(:,[2,4]) = [];


idxTurtle = strcmp('turtle',species);
idxLizard = strcmp('lizard',species);
xValues = (1:size(ftr,2))';

xbin = [1,2;...
        3,4;...
        5,6;...
        7,8;...
        9,10;...
        11,12];
xo = (0:0.5:2.5)';    


figure('color','w');
ht = violin(ftr(idxTurtle,:),...
       'mc',[],'medc',[],...
       'facecolor',[.35,.35,.35],...
       'edgecolor',[0,0,0],...
       'facealpha',1);

for k = 1 : length(ht)
    
    ht(k).XData = (ht(k).XData - k) + xbin(k,1) + xo(k);
    
end
  
hl = violin(ftr(idxLizard,:),...
       'mc',[],'medc',[],...
       'facecolor',[.85,.85,.85],...
       'edgecolor',[.25,.25,.25],...
       'facealpha',1);
xoffset = (1:6);
for k = 1 : length(hl)
    
    hl(k).XData = (hl(k).XData - k) + xbin(k,2) + xo(k);
    
end

h = [ht(1),hl(1)];

xtick = xbin+xo + 0.5;
hl = legend(h,'turtle','lizard');
set(hl,'edgecolor','w','location','northeast');
set(gca,'box','off',...
        'fontsize',10,...
        'xlim',[0,15],...
        'xtick',xtick(:,1),...
        'xticklabel',lbl,...
        'xticklabelrotation',45,...
        'ylim',[0,50],...
        'ticklength',[0.01,0.01]);
ylabel('relative fraction of cell reads [%]');  
       
%{
figure('Color','w');
hold on;
plotSpread(ftr(idxTurtle,:),'spreadWidth',0.3,'distributionColor',[0,0,0],'xValues',xValues-0.2);
plotSpread(ftr(idxLizard,:),'spreadWidth',0.3,'distributionColor',[0.65,0.65,0.65],'xValues',xValues+0.2);
hold off;
h = get(gca,'Children');
idxHt = strcmp('0.8',{h.DisplayName});
idxHl = strcmp('1.2',{h.DisplayName});
hc = [h(idxHt);h(idxHl)];
hl = legend(hc,'turtle', 'lizard');
set(hl,'edgecolor','w','location','northwest');
set(gca,'box','off',...
        'xlim',[.5,size(ftr,2)+.5],...
        'xtick',xValues,...
        'xticklabel',lbl,...
        'xticklabelrotation',45,...
        'ylim',[0,50]);
ylabel('relative fraction of cell reads [%]');
%}
print(gcf,'-dpng','-r300','../figures/figureX_DistributionOfGeneFeatures.png');
print(gcf,'-dsvg','-r300','../figures/figureX_DistributionOfGeneFeatures.svg');

%}
%% Good cells associated reads
%{
cid = 100.*counts(:,3)./counts(:,2);
cid_t = median(cid(strcmp('turtle', species)));
cid_l = median(cid(strcmp('lizard', species)));


figure('color','w');
h = plotSpread(cid,...
          'distributionIdx',species,...
          'distributionColors',[0,0,0;0.65,0.65,0.65],...
          'xValues',[1,2]);
hc = get(h{3}, 'Children');
set(hc, 'MarkerSize', 10);
hold on;
plot([0.75,1.25],[cid_t,cid_t],'r');
plot([1.75,2.25],[cid_l,cid_l],'r');
hold off;
set(gca,'box','off',...
        'xlim',[0.5,2.5],...
        'ylim',[0,100]);
ylabel('relative fraction of uniq. aligned reads [%]');    
title('distribution of cell associated uniq. aligned reads','fontweigh','normal');
print(gcf,'-dpng','-r300','figureX_DistributionOfCellAssociatedReads.png');
print(gcf,'-dsvg','-r300','figureX_DistributionOfCellAssociatedReads.svg');
%}


