
# comparative WGCNA - code adapted from WGCNA tutorials
# comparison of mouse and turtle interneurons. 
# the same pipeline was used to compare other cell types (see Supplementary Material for analysis parameters)

library(WGCNA)

load("./turtle.neurons.Robj")
load("./tasic.Robj") # mouse data from Tasic et al 2016
source("./MeanVarData.R")

# requires files with gene functional annotations from eggnog


##### prepare interneuron expression tables #####
#################################################

# take turtle interneuron expression data
turtle_clusters <- c(paste0("i0",seq(7,9,by=1)),paste0("i", seq(10,18,by=1))) # select GABAergic clusters (MGE and CGE-derived)
turtle_ids <- names(turtle.neurons@ident[turtle.neurons@ident %in% turtle_clusters])
turtle_all.data <- turtle.neurons@data[,turtle_ids]
turtle_all.data <- Matrix(turtle_all.data, sparse=F)
turtle_all.data <- as.matrix(turtle_all.data)
# 21167   368
rownames(turtle_all.data) = toupper(rownames(turtle_all.data))

# take mouse interneuron expression data from the Tasic dataset
tasic_clusters <- c("f01","f02","f03","f04","f05","f06","f07","f08","f09","f10","f11","f12","f13","f14","f15","f16","f17","f18","f19","f20","f21","f22","f23")
tasic_all.ids <- names(tasic@ident[tasic@ident %in% tasic_clusters])

tasic_ids <- tasic_all.ids
tasic_all.data <- tasic@data[,tasic_ids]
tasic_all.data <- Matrix(tasic_all.data, sparse=F)
tasic_all.data <- as.matrix(tasic_all.data)
# 19625   761
rownames(tasic_all.data) = toupper(rownames(tasic_all.data))


# create pseudocells
##################################################

pseudocell.size = 5

turtle_new_ids_list = list()
for (i in 1:length(levels(turtle_ident))) {
	cluster_id = levels(turtle_ident)[i]
	cluster_cells <- names(turtle_ident[turtle_ident == cluster_id])
	cluster_size <- length(cluster_cells)		
	pseudo_ids <- floor(seq_along(cluster_cells)/pseudocell.size)
	pseudo_ids <- paste0(cluster_id, "_", pseudo_ids)
	names(pseudo_ids) <- sample(cluster_cells)	
	turtle_new_ids_list[[i]] <- pseudo_ids		
	}	
turtle_new_ids <- unlist(turtle_new_ids_list)
turtle_new_ids <- as.factor(turtle_new_ids)
turtle_new_ids_length <- table(turtle_new_ids)
turtle_short_ids <- names(turtle_new_ids_length[turtle_new_ids_length<3])
turtle_all_ids <- levels(turtle_new_ids)  # 514
turtle_good_ids <- turtle_all_ids[!turtle_all_ids %in% turtle_short_ids]  

turtle_cells <- names(turtle_new_ids)[turtle_new_ids %in% turtle_good_ids]
turtle_final_ids <- turtle_new_ids[turtle_cells]
turtle_final_ids <- droplevels(turtle_final_ids)	 
# 359 cells, 75 pseudocells, 1 with 3 cells, 14 with 4 cells and 60 with 5 cells 

tasic_new_ids_list = list()
for (i in 1:length(levels(tasic_ident))) {
	cluster_id = levels(tasic_ident)[i]
	cluster_cells <- names(tasic_ident[tasic_ident == cluster_id])
	cluster_size <- length(cluster_cells)		
	pseudo_ids <- floor(seq_along(cluster_cells)/pseudocell.size)
	pseudo_ids <- paste0(cluster_id, "_", pseudo_ids)
	names(pseudo_ids) <- sample(cluster_cells)	
	tasic_new_ids_list[[i]] <- pseudo_ids		
	}	
tasic_new_ids <- unlist(tasic_new_ids_list)
tasic_new_ids <- as.factor(tasic_new_ids)
tasic_new_ids_length <- table(tasic_new_ids)
tasic_short_ids <- names(tasic_new_ids_length[tasic_new_ids_length<3])
tasic_all_ids <- levels(tasic_new_ids)  # 514
tasic_good_ids <- tasic_all_ids[!tasic_all_ids %in% tasic_short_ids]  

tasic_cells <- names(tasic_new_ids)[tasic_new_ids %in% tasic_good_ids]
tasic_final_ids <- tasic_new_ids[tasic_cells]
tasic_final_ids <- droplevels(tasic_final_ids)	 
# 352 cells, 76 pseudocells, 28 with 4 cells and 48 with 5 cells 


turtle_all.data <- turtle.neurons@data[,turtle_cells]
turtle_all.data<-Matrix(turtle_all.data, sparse=F)
turtle_all.data<-as.matrix(turtle_all.data)
expMean=function(x) {
  		return(log(mean(exp(x)-1)+1))
		} 
turtle_pdata = NULL
turtle_data_t <- turtle_all.data
for(i in levels(turtle_final_ids)) {
              temp.cells= names(turtle_final_ids[turtle_final_ids == i])
              if (length(temp.cells)==1) data.temp=(turtle_data_t[,temp.cells])
              if (length(temp.cells)>1) data.temp=apply(turtle_data_t[,temp.cells],1,expMean)
              turtle_pdata =cbind(turtle_pdata,data.temp)
              colnames(turtle_pdata)[ncol(turtle_pdata)]=i
            }
turtle_all.data <- turtle_pdata
# 21167    75

tasic_all.data <- tasic@data[,tasic_cells]
tasic_all.data<-Matrix(tasic_all.data, sparse=F)
tasic_all.data<-as.matrix(tasic_all.data)
expMean=function(x) {
  		return(log(mean(exp(x)-1)+1))
		} 
tasic_pdata = NULL
tasic_data_t <- tasic_all.data
for(i in levels(tasic_final_ids)) {
              temp.cells= names(tasic_final_ids[tasic_final_ids == i])
              if (length(temp.cells)==1) data.temp=(tasic_data_t[,temp.cells])
              if (length(temp.cells)>1) data.temp=apply(tasic_data_t[,temp.cells],1,expMean)
              tasic_pdata =cbind(tasic_pdata,data.temp)
              colnames(tasic_pdata)[ncol(tasic_pdata)]=i
            }
tasic_all.data <- tasic_pdata
# 19625    76


# find one-to-one orthologs, replace gene names with one-to-one orthologs names 
###############################################################################

rownames(turtle_all.data) <- toupper(rownames(turtle_all.data))
rownames(tasic_all.data) <- toupper(rownames(tasic_all.data))

eggnog1 = read.table('chrysemys_eggnog_pruned.txt',header=F,stringsAsFactors = F)
eggnog1[2:nrow(eggnog1),1] = toupper(eggnog1[2:nrow(eggnog1),1])
eggnog2 = read.table('mus_eggnog_pruned.txt',header=F,stringsAsFactors = F)
eggnog2[2:nrow(eggnog2),1] = toupper(eggnog2[2:nrow(eggnog2),1])
genesSpecies1 = rownames(turtle_all.data)
genesSpecies2 = rownames(tasic_all.data)
colnames(eggnog1) = eggnog1[1,]
eggnog1 = eggnog1[2:nrow(eggnog1),]
colnames(eggnog2) = eggnog2[1,]
eggnog2 = eggnog2[2:nrow(eggnog2),]
genesSpecies1 = eggnog1[eggnog1[,1] %in% genesSpecies1,2]
genesSpecies2 = eggnog2[eggnog2[,1] %in% genesSpecies2,2]
genes = intersect(genesSpecies1,genesSpecies2)   #11672 genes

genesSpecies1 = eggnog1[eggnog1[,2] %in% genes,1]
genesSpecies2 = eggnog2[eggnog2[,2] %in% genes,1]
turtle_all.data <- turtle_all.data[rownames(turtle_all.data) %in% genesSpecies1,]
tasic_all.data <- tasic_all.data[rownames(tasic_all.data) %in% genesSpecies2,]

eggnog1.f <- eggnog1[eggnog1[,1] %in% rownames(turtle_all.data),]
turtle_all.data <- turtle_all.data[order(match(rownames(turtle_all.data),eggnog1.f[,1] )),]
all.equal(eggnog1.f[,1], rownames(turtle_all.data))
rownames(turtle_all.data) <- eggnog1.f[,2]

eggnog2.f <- eggnog2[eggnog2[,1] %in% rownames(tasic_all.data),]
tasic_all.data <- tasic_all.data[order(match(rownames(tasic_all.data),eggnog2.f[,1] )),]
all.equal(eggnog2.f[,1], rownames(tasic_all.data))
rownames(tasic_all.data) <- eggnog2.f[,2]

turtle_all.data <- turtle_all.data[order(rownames(turtle_all.data), decreasing=F),]
# 11672    75
tasic_all.data <- tasic_all.data[order(rownames(tasic_all.data), decreasing=F),]
# 11672    76



# select genes with high variance in both datasets
##################################################

# remove non expressed genes.
turtle_all.means <- rowMeans(turtle_all.data)
tasic_all.means <- rowMeans(tasic_all.data)
genes.keep <- intersect(names(turtle_all.means[turtle_all.means>0]), names(tasic_all.means[tasic_all.means>0]))
# 10734 genes
genes.keep <- genes.keep[order(genes.keep, decreasing=F)]
turtle_all.data <- turtle_all.data[genes.keep,]
tasic_all.data <- tasic_all.data[genes.keep,]

genes.use <- genes.keep

# find variable genes         
turtle_meanvar <- MeanVarData(turtle_all.data)
tasic_meanvar <- MeanVarData(tasic_all.data)
 
# remove noisy genes
turtle_noisy_genes <- NoisyGenes(neur, min.pct=0.4, clusters.use= turtle_clusters)
tasic_noisy_genes <- NoisyGenes(tasic, min.pct=0.4, clusters.use= tasic_clusters)

NoisyGenes1 = eggnog1[eggnog1[,2] %in% turtle_noisy_genes,1]
NoisyGenes2 = eggnog2[eggnog2[,2] %in% toupper(tasic_noisy_genes),1]

turtle_meanvar.f <- turtle_meanvar[!rownames(turtle_meanvar) %in% NoisyGenes1, ]   
tasic_meanvar.f <- tasic_meanvar[!rownames(tasic_meanvar) %in% NoisyGenes2, ]    

# order the filtered table of variable genes by the DM metric 
turtle_meanvar.f <- turtle_meanvar.f[order(turtle_meanvar.f[,5], decreasing=T),] 
tasic_meanvar.f <- tasic_meanvar.f[order(tasic_meanvar.f[,5], decreasing=T),] 

turtle_top.genes <- rownames(turtle_meanvar.f)
turtle_top.genes <- turtle_top.genes[!turtle_top.genes %in% NoisyGenes2]
tasic_top.genes <- rownames(tasic_meanvar.f)
tasic_top.genes <- tasic_top.genes[!tasic_top.genes %in% NoisyGenes1]

# take the top 750 variable genes in each dataset
final.genes <- unique(c(turtle_top.genes[1:750],  tasic_top.genes[1:750]))
length(final.genes)
# 1208 genes


#################
#### datasets ###
#################
 
turtle_data <- t(turtle_all.data[rownames(turtle_all.data) %in% final.genes,])
tasic_data <- t(tasic_all.data[rownames(tasic_all.data) %in% final.genes,])


##### WGCNA turtle #####
########################

Date = 170904
name = "turtle_test18.1"
SubGeneNames=colnames(turtle_data)

powers = c(c(1:18), seq(from = 20, to=40, by=2))
sft = pickSoftThreshold(turtle_data, powerVector = powers, verbose = 5,networkType = "signed")

pdf(paste0(Date, "_",name, "_soft_threshold.pdf"), width=12)
par(mfrow = c(1,2));
cex1 = 0.9
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
     xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
    main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
    labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.90,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5],
    xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
    main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
dev.off()

turtle_softPower = 6
NetworkType = "signed"
turtle_adj= adjacency(turtle_data,type = NetworkType, power = turtle_softPower)
turtle_k <- softConnectivity(turtle_data, type="signed", power=turtle_softPower)
turtle_k <- turtle_k/max(turtle_k)
names(turtle_k) <- colnames(turtle_data)
quantile(turtle_k, probs=seq(0,1,by=0.1))
 

##### WGCNA tasic ######
########################

Date = 170904
name = "tasic_test18.1"
SubGeneNames=colnames(tasic_data)

powers = c(c(1:18), seq(from = 20, to=40, by=2))
sft = pickSoftThreshold(tasic_data, powerVector = powers, verbose = 5,networkType = "signed")

pdf(paste0(Date, "_",name, "_soft_threshold.pdf"), width=12)
par(mfrow = c(1,2));
cex1 = 0.9
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
     xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
    main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
    labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.90,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5],
    xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
    main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
dev.off()

tasic_softPower = 7
NetworkType = "signed"
tasic_adj= adjacency(tasic_data,type = NetworkType, power = tasic_softPower)
tasic_k <- softConnectivity(tasic_data, type="signed", power = tasic_softPower)
tasic_k <- tasic_k/max(tasic_k)
names(tasic_k) <- colnames(tasic_data)
quantile(tasic_k, probs=seq(0,1,by=0.1))


######## find modules turtle ######
###################################

Date = 170904
name = "turtle_test18.1"

turtle_TOM=TOMsimilarityFromExpr(turtle_data,networkType = NetworkType, TOMType = "signed", power = turtle_softPower)
colnames(turtle_TOM) =rownames(turtle_TOM) =SubGeneNames
# save(TOM, file="turtle_all_highGenes_TOMsigned.Robj")
turtle_dissTOM = 1-turtle_TOM
turtle_geneTree = flashClust(as.dist(turtle_dissTOM),method="average")

# pdf(paste0(Date,"_",test,"_dendrogram_",NetworkType,"_power", turtle_softPower,".pdf", sep=""), width=12)
# plot(turtle_geneTree, xlab="", sub="",cex=0.3, labels=F)
# dev.off()

turtle_minModuleSize = 30
turtle_dynamicMods = cutreeDynamic(dendro = turtle_geneTree, distM = turtle_dissTOM, method="hybrid", deepSplit = 4, pamRespectsDendro = FALSE, minClusterSize = turtle_minModuleSize, pamStage=T)
turtle_dynamicColors = labels2colors(turtle_dynamicMods)
table(turtle_dynamicColors)

pdf(paste0(Date,"_",name,"_modules_",NetworkType, "_moduleSize",turtle_minModuleSize,"_power", turtle_softPower,".pdf", sep=""), width=12)
plotDendroAndColors(turtle_geneTree, turtle_dynamicColors, "Dynamic Tree Cut", dendroLabels = FALSE, hang = 0.03, addGuide = TRUE, guideHang = 0.05, main = "Gene dendrogram and module colors")
dev.off()

# diag(turtle_dissTOM) = NA


# module merging

turtle_MEList = moduleEigengenes(turtle_data, colors = turtle_dynamicColors)
turtle_MEs = turtle_MEList$eigengenes

turtle_MEDiss = 1-cor(turtle_MEs);
turtle_METree = hclust(as.dist(turtle_MEDiss), method = "average");

pdf(paste0(Date,"_",name,"_hclust_modules_",NetworkType, "moduleSize",turtle_minModuleSize,"_power", turtle_softPower,".pdf", sep=""), width=12)
plot(turtle_METree, main = "Clustering of module eigengenes",
xlab = "", sub = "")
dev.off()

turtle_MEDissThres = 0.5
# Call an automatic merging function
turtle_merge = mergeCloseModules(turtle_data, turtle_dynamicColors, cutHeight = turtle_MEDissThres, verbose = 3)
# The merged module colors
turtle_mergedColors = turtle_merge$colors;
# Eigengenes of the new merged modules:
turtle_mergedMEs = turtle_merge$newMEs; 

pdf(paste0(Date,"_",name,"_merged_modules_",NetworkType, "moduleSize",turtle_minModuleSize,"_power", turtle_softPower,".pdf", sep=""), width=12)
plotDendroAndColors(turtle_geneTree, cbind(turtle_dynamicColors, turtle_mergedColors),
c("Dynamic Tree Cut", "Merged dynamic"),
dendroLabels = FALSE, hang = 0.03,
addGuide = TRUE, guideHang = 0.05)
dev.off()

turtle_dynamicColors <- turtle_mergedColors


######## find modules tasic ######
###################################

Date = 170904
name = "tasic_test18.1"

tasic_TOM=TOMsimilarityFromExpr(tasic_data,networkType = NetworkType, TOMType = "signed", power = tasic_softPower)
colnames(tasic_TOM) =rownames(tasic_TOM) =SubGeneNames
# save(TOM, file="turtle_all_highGenes_TOMsigned.Robj")
tasic_dissTOM=1-tasic_TOM
tasic_geneTree = flashClust(as.dist(tasic_dissTOM),method="average")

# pdf(paste0(Date,"_",test,"_dendrogram_",NetworkType,"_power", softPower,".pdf", sep=""), width=12)
# plot(tasic_geneTree, xlab="", sub="",cex=0.3, labels=F)
# dev.off()

tasic_minModuleSize = 30
tasic_dynamicMods = cutreeDynamic(dendro = tasic_geneTree, distM = tasic_dissTOM, method="hybrid", deepSplit = 4, pamRespectsDendro = FALSE, minClusterSize = tasic_minModuleSize, pamStage=T)
tasic_dynamicColors = labels2colors(tasic_dynamicMods)
table(tasic_dynamicColors)

pdf(paste0(Date,"_",name,"_modules_",NetworkType, "moduleSize",tasic_minModuleSize,"_power", tasic_softPower,".pdf", sep=""), width=12)
plotDendroAndColors(tasic_geneTree, tasic_dynamicColors, "Dynamic Tree Cut", dendroLabels = FALSE, hang = 0.03, addGuide = TRUE, guideHang = 0.05, main = "Gene dendrogram and module colors")
dev.off()

# diag(tasic_dissTOM) = NA


# module merging

tasic_MEList = moduleEigengenes(tasic_data, colors = tasic_dynamicColors)
tasic_MEs = tasic_MEList$eigengenes

tasic_MEDiss = 1-cor(tasic_MEs);
tasic_METree = hclust(as.dist(tasic_MEDiss), method = "average");

pdf(paste0(Date,"_",name,"_hclust_modules_",NetworkType, "moduleSize",tasic_minModuleSize,"_power", tasic_softPower,".pdf", sep=""), width=12)
plot(tasic_METree, main = "Clustering of module eigengenes",
xlab = "", sub = "")
dev.off()

tasic_MEDissThres = 0.5
# Call an automatic merging function
tasic_merge = mergeCloseModules(tasic_data, tasic_dynamicColors, cutHeight = tasic_MEDissThres, verbose = 3)
# The merged module colors
tasic_mergedColors = tasic_merge$colors;
# Eigengenes of the new merged modules:
tasic_mergedMEs = tasic_merge$newMEs; 

pdf(paste0(Date,"_",name,"_merged_modules_",NetworkType, "moduleSize",tasic_minModuleSize,"_power", tasic_softPower,".pdf", sep=""), width=12)
plotDendroAndColors(tasic_geneTree, cbind(tasic_dynamicColors, tasic_mergedColors),
c("Dynamic Tree Cut", "Merged dynamic"),
dendroLabels = FALSE, hang = 0.03,
addGuide = TRUE, guideHang = 0.05)
dev.off()

tasic_dynamicColors <- tasic_mergedColors


######## module eigengenes turtle ######
########################################

Date = 170904
name = "turtle_test18.1"

turtle_MEList = moduleEigengenes(turtle_data, colors = turtle_dynamicColors)
turtle_MEs = turtle_MEList$eigengenes

turtle_MEeigengenes <- as.matrix(turtle_MEList$eigengenes)
rownames(turtle_MEeigengenes) <- rownames(turtle_data)
turtle_MEeigengenes <- turtle_MEeigengenes[order(match(rownames(turtle_MEeigengenes), names(turtle_ident))),]
turtle_MEeigengenes <- turtle_MEeigengenes[,!colnames(turtle_MEeigengenes) %in% c("MEgrey")]

turtle_tempident <- rownames(turtle_MEeigengenes)
turtle_tempident <- sapply(turtle_tempident, function(x) substring(x,1,3))
names(turtle_tempident) <- rownames(turtle_MEeigengenes)
turtle_tempident <- as.factor(turtle_tempident)
turtle_tempident <- factor(turtle_tempident, levels(turtle_tempident)[c(8:12,1:4,6,5,7)])
turtle_tempident <- as.data.frame(turtle_tempident)
turtle_tempident <- cbind(turtle_tempident, rownames(turtle_tempident))
colnames(turtle_tempident) <- c("tempident","turtle_id")
turtle_tempident <- arrange(turtle_tempident, tempident)
turtle_tempident2 <- as.factor(turtle_tempident[,1])
names(turtle_tempident2) <- turtle_tempident[,2]
turtle_tempident <- turtle_tempident2


n.clusters = length(table(turtle_tempident))
plot_colors = colorRampPalette(brewer.pal(9, "Set1"))(n.clusters)
turtle_plotColors <- mapvalues(turtle_tempident, from = levels(turtle_tempident), to = plot_colors) # plyr package
turtle_plotColors <- as.character(turtle_plotColors)

turtle_MEeigengenes[turtle_MEeigengenes>0.3] <- NA
turtle_MEeigengenes <- turtle_MEeigengenes[order(match(rownames(turtle_MEeigengenes), names(turtle_tempident))),]
turtle_MEeigengenes <- turtle_MEeigengenes[,c("MEbrown","MEturquoise","MEblue")]


pdf(paste0(Date,"_",name,"_eigengenes_",NetworkType, "moduleSize",turtle_minModuleSize,"_power", turtle_softPower,".pdf", sep=""), width=13)
par(oma=c(4,4,4,10))
colors <- colorRampPalette(c("#9D8BFF","black", "#FFE000"))(n = 200)
pal <- colorRampPalette(colors)
heatmap.2(t(turtle_MEeigengenes), Colv=F, Rowv=F, trace="none",  col=pal, ColSideColors= turtle_plotColors,na.color="green")
dev.off()

# extract names of genes that belong to the modules
SubGeneNames=colnames(turtle_data)
turtle_module_colors= setdiff(unique(turtle_dynamicColors), "grey")
for (color in turtle_module_colors){
    module=SubGeneNames[which(turtle_dynamicColors==color)]
    write.table(module, paste(Date, name, "_module_",color, ".txt",sep=""), sep="\t", row.names=FALSE, col.names=FALSE,quote=FALSE)    
}



######## module eigengenes tasic #######
########################################

Date = 170904
name = "tasic_test18.1"

tasic_MEList = moduleEigengenes(tasic_data, colors = tasic_dynamicColors)
tasic_MEs = tasic_MEList$eigengenes
tasic_MEeigengenes <- as.matrix(tasic_MEList$eigengenes)
rownames(tasic_MEeigengenes) <- rownames(tasic_data)
tasic_MEeigengenes <- tasic_MEeigengenes[order(match(rownames(tasic_MEeigengenes), names(tasic_ident))),]
tasic_MEeigengenes <- tasic_MEeigengenes[,!colnames(tasic_MEeigengenes) %in% c("MEgrey")]


tasic_tempident <- rownames(tasic_MEeigengenes)
tasic_tempident <- sapply(tasic_tempident, function(x) substring(x,1,3))
names(tasic_tempident) <- rownames(tasic_MEeigengenes)
tasic_tempident <- as.factor(tasic_tempident)
tasic_tempident <- factor(tasic_tempident, levels(tasic_tempident)[c(1:5, 10, 6,7,9,8,11,16,12:15,17:23)])
tasic_tempident <- as.data.frame(tasic_tempident)
tasic_tempident <- cbind(tasic_tempident, rownames(tasic_tempident))
colnames(tasic_tempident) <- c("tempident","tasic_id")
tasic_tempident <- arrange(tasic_tempident, tempident)
tasic_tempident2 <- as.factor(tasic_tempident[,1])
names(tasic_tempident2) <- tasic_tempident[,2]
tasic_tempident <- tasic_tempident2


n.clusters = length(table(tasic_tempident))
plot_colors = colorRampPalette(brewer.pal(9, "Set1"))(n.clusters)
tasic_plotColors <- mapvalues(tasic_tempident, from = levels(tasic_tempident), to = plot_colors) # plyr package
tasic_plotColors <- as.character(tasic_plotColors)

tasic_MEeigengenes[tasic_MEeigengenes>0.3] <- NA  
tasic_MEeigengenes <- tasic_MEeigengenes[order(match(rownames(tasic_MEeigengenes), names(tasic_tempident))),]
tasic_MEeigengenes <- tasic_MEeigengenes[,c("MEbrown","MEyellow","MEblue")]

pdf(paste0(Date,"_",name,"_eigengenes_",NetworkType, "moduleSize",tasic_minModuleSize,"_power", tasic_softPower,".pdf", sep=""), width=13)
par(oma=c(4,4,4,10))
colors <- colorRampPalette(c("#9D8BFF","black", "#FFE000"))(n = 200)
pal <- colorRampPalette(colors)
heatmap.2(t(tasic_MEeigengenes), Colv=F, Rowv=F, trace="none",  col=pal, ColSideColors= tasic_plotColors, na.color="green")
dev.off()

# extract names of genes that belong to the modules
SubGeneNames=colnames(tasic_data)
tasic_module_colors= setdiff(unique(tasic_dynamicColors), "grey")
for (color in tasic_module_colors){
    module=SubGeneNames[which(tasic_dynamicColors==color)]
    write.table(module, paste(Date, name, "_module_",color, ".txt",sep=""), sep="\t", row.names=FALSE, col.names=FALSE,quote=FALSE)    
}




######## compare modules fast ######
####################################

Date = 170904
name = "test18"

inNetwork = tasic_k> 0 | turtle_k> 0

colorh = as.character(turtle_dynamicColors)
colorhALL=rep("grey", length(turtle_k))
colorhALL[inNetwork]=as.character(colorh)
colorh2 =as.character(tasic_dynamicColors)
colorh2ALL=rep("grey", length(tasic_k))
colorh2ALL[inNetwork]=as.character(colorh2)
colorTurtle = colorhALL;
colortasic = colorh2ALL;

nSets = 2
multiExpr = list()
multiExpr[[1]] = list(data=turtle_data)
multiExpr[[2]] = list(data=tasic_data)
setLabels = c("turtle","tasic")
names(multiExpr) = setLabels

colorList = list(colorTurtle, colortasic)
names(colorList) = setLabels

ref=1
test=2

dendrograms = list();
for (set in 1:nSets)
{
  adj = abs(cor(multiExpr[[set]]$data[, inNetwork], use = "p"))^9;
  dtom = TOMdist(adj);
  dendrograms[[set]] = flashClust(as.dist(dtom), method = "a");
}
# Get eigengenes
mes = list()
for (set in 1:nSets)
{
   mes[[set]] = moduleEigengenes(multiExpr[[set]]$data, colorList[[ref]])$eigengenes
}
 

overlap = overlapTable(colorTurtle[inNetwork], colortasic[inNetwork]);

# The numMat will encode color. We use -log of the p value.
numMat = -log10(overlap$pTable);
numMat[numMat >50] = 50;
# Prepare for generating a color-coded plot of the overlap table. The text of the table will consist of
# counts and corresponding p-values.
textMat = paste(overlap$countTable, "\n", signif(overlap$pTable, 2));
dim(textMat) = dim(numMat)
# Additional information for the plot. These will be used shortly.
xLabels = paste("M", sort(unique(colorTasic)));
yLabels = paste("M", sort(unique(colorTurtle)));
xSymbols = paste(sort(unique(colortasic)), ": ", table(colortasic[inNetwork]), sep = "")
ySymbols = paste(sort(unique(colorTurtle)), ": ", table(colorTurtle[inNetwork]), sep = "")

pdf(paste0(Date,"interneurons",name,"module_comparison.pdf", sep=""))
# par(oma=c(10,10,10,10))
fp=TRUE
# Plot the overlap table
fcex = 1.00;
pcex = 1.0
fcexl = 1.00;
pcexl = 1.00;
par(mar = c(6, 7, 2, 1.0));
labeledHeatmap(Matrix = numMat,
xLabels = xLabels, xSymbols = xSymbols,
yLabels = yLabels, ySymbols = ySymbols,
colorLabels = TRUE,
colors = colorRampPalette(c("white","goldenrod1"))(50),
textMatrix = textMat, cex.text = if (fp) fcex else pcex, setStdMargins = FALSE,
cex.lab = if (fp) fcexl else pcexl,
xColorWidth = 0.08,
main = "Turtle modules (rows) vs. tasic modules (columns)", cex.main = 1.2);
dev.off()

