# Reptile Pallium
Analysis pipeline for the data reported in "Evolution of pallium, hippocampus and cortical cell types revealed by single-cell transcriptomics in reptiles" by Tosches, Yamawaki, Naumann, Jacobi, Tushev and Laurent (2018)

## Contents

* **Extension of 3'UTRs**

  Contains code used to update the annotation of the *Chrysemys picta* and *Pogona vitticeps* genomes from 3'MACE data. Requires matlab.

* **Single-cell data preprocessing and quality control**

  Contains R scripts used to filter out bad-quality cells from drop-seq data. Requires the R packages e1071(v1.6-8) and Seurat (v1.4).

* **Dimensionality reduction, clustering and differential gene expression analysis**

  Contains scripts used to analyse turtle and lizard single-cell data. This code uses the R packages Seurat (v1.4), rpca (v0.2.3), bigpca(v1.0.3), data.table(v1.10.4), and MAST(v1.0.5).

* **Gene network analysis**

  Scripts used for WGCNA analysis. Requires R package WGCNA(v1.43-10)

* **Comparison of single-cell transcriptomes across species**

  Contains R scripts used to compare reptilian and mammalian cell types. 


## Notes

Please note that the R scripts uploaded here were written for the Seurat version 1.4 (October 2016; see http://satijalab.org/seurat/) running on R 3.4.0. In more recent versions of Seurat, some object slots have been renamed. This means that this code needs to be updated to run with recent versions of Seurat. We publish the code here in the interest of transparency and reproducibility, but we are unable to provide further support on code development. 


## Downloads

Preprocessed data and other files necessary for this analysis can be downloaded from http://brain.mpg.de/research/laurent-department/software-techniques.html.
